from src.first_simulation.experiment import FirstExperiment
from src.second_simulation.experiment import SecondExperiment

if __name__ == '__main__':
    # THE FIRST EXPERIMENT
    for alpha in [.0, .2, .4, .6, .8, 1.]:
        FirstExperiment(num_steps=10, num_mc_runs=100000, alpha=alpha, tau=200).run()

    # THE SECOND EXPERIMENT
    SecondExperiment(num_steps=10, num_mc_runs=100000).run()
