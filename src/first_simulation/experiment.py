from pathlib import Path
from typing import Dict

import numpy as np
from matplotlib import pyplot as plt

from src.first_simulation.models import BaseAgent, Environment, FusionAgent, NeighbourAgent

# Simulation settings of parameter w
SEED = 1
W_STEP = .02
W_MAX = 1


class FirstExperiment:

    def __init__(self, num_steps: int, num_mc_runs: int, alpha: float, tau: int) -> None:
        """
        Initialize experiment.
        """
        self.w_span = np.arange(0, W_MAX + W_STEP, W_STEP)
        self.num_steps = num_steps
        self.num_mc_runs = num_mc_runs
        self.alpha = alpha
        self.tau = tau

    def _get_averaged_kld(self, w: float) -> np.ndarray:
        """
        Return an average of Kullback-Leibler divergence over the number of Monte Carlo runs.
        """
        avg_kld = np.zeros((1, 3), dtype=float)
        for i in range(self.num_mc_runs):
            avg_kld += MonteCarloSimulation(w, self.num_steps, self.alpha, self.tau).perform_single_run()
        return avg_kld / self.num_mc_runs

    def run(self) -> None:
        """
        Run experiment.
        """
        results = {}
        for w in self.w_span:
            results[w] = self._get_averaged_kld(w)
        self.plot_results(results)

    def plot_results(self, results: Dict[float, np.ndarray]) -> None:
        """
        Plot figures.
        """
        fontsize = 16
        ext_agent_kld = []
        base_agent_kld = []
        agent_kld = []

        for w in self.w_span:
            ext_agent_kld.append(results[w][0][0])
            base_agent_kld.append(results[w][0][1])
            agent_kld.append(results[w][0][2])

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(self.w_span, base_agent_kld, c='b', ls='--', linewidth=4)
        ax.plot(self.w_span, agent_kld, c='r', ls='-', linewidth=4)
        plt.xlabel(r'$\mathsf{w}$', fontsize=fontsize)
        plt.ylabel('KLD', fontsize=fontsize)
        plt.xticks(fontsize=fontsize)
        plt.yticks(fontsize=fontsize)
        plt.grid()
        plt.savefig(Path(__file__).parent / f'figures/results_for_alpha_{self.alpha.__str__().replace(".", "-")}.png')
        plt.show()


class MonteCarloSimulation:

    def __init__(self, w: float, num_steps: int, alpha: float, tau: int) -> None:
        """
        Initialize a single Monte Carlo simulation.
        """
        self.w = w
        self.num_steps = num_steps
        self.tau = tau
        self.environment = Environment(alpha=alpha)
        self.ext_agent = NeighbourAgent()
        self.base_agent = BaseAgent()
        self.agent = FusionAgent()

    def perform_single_run(self) -> np.ndarray:
        """
        Perform a single run for the given number of decision steps and return a final Kullback-Leibler divergence.
        """
        for step in range(self.num_steps):
            if step == 0:
                self.agent.update_occurrence_table_with_fusion(self.ext_agent, self.w, self.tau)
            action = self.environment.action()
            observable_state = self.environment.state()
            self.agent.update_occurrence_table(action, observable_state)
            self.base_agent.update_occurrence_table(action, observable_state)
        return self._compute_kld()

    def _compute_kld(self) -> np.ndarray:
        """
        Compute Kullback_Leibler divergences.
        """
        kld = np.zeros((self.ext_agent.num_actions, 3), dtype=float)
        for action in range(self.ext_agent.num_actions):
            env_prob = self._to_probabilities(self.environment.occurrence_table[action])
            kld[action][0] = sum(np.multiply(env_prob, np.divide(env_prob, self._to_probabilities(self.ext_agent.occurrence_table[action]))))
            kld[action][1] = sum(np.multiply(env_prob, np.divide(env_prob, self._to_probabilities(self.base_agent.occurrence_table[action]))))
            kld[action][2] = sum(np.multiply(env_prob, np.divide(env_prob, self._to_probabilities(self.agent.occurrence_table[action]))))
        return kld

    @staticmethod
    def _to_probabilities(occurrences: np.ndarray) -> np.ndarray:
        """
        Normalize occurrence table $V_{o_{\M{a}}}$ to probabilities.
        """
        return occurrences / sum(occurrences)
