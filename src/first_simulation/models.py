import numpy as np

# Auxiliary vectors $v_{\M{a}}$ and $v_{\M{n}}$
NEIGHBOUR_VECTOR = [1, 1, 1, 1, 1, 3, 9, 2, 1]
AGENT_VECTOR = [1, 2, 9, 3, 1, 1, 1, 1, 1]
N_VEC = np.array([NEIGHBOUR_VECTOR], dtype=float)
A_VEC = np.array([AGENT_VECTOR], dtype=float)


class Environment:

    def __init__(self, alpha: float):
        """
        Initialize environment class.
        """
        self.occurrence_table = self.initialize_prior_occurrence_table(alpha)
        self.num_actions, self.num_states = self.occurrence_table.shape

    @staticmethod
    def initialize_prior_occurrence_table(ext_affinity: float) -> np.ndarray:
        """
        Initialize prior occurrence table $V_{o_{\M{a}}}$.
        This occurrence table uniquely determines the environment.
        """
        return ext_affinity * N_VEC.copy() + (1 - ext_affinity) * A_VEC.copy()

    def action(self) -> int:
        """
        Generate and return an dt.
        """
        return np.random.choice([a for a in range(self.num_actions)], 1)[0]

    def state(self) -> int:
        """
        Generate and return an observable dt_1.
        """
        action = self.action()
        probabilities = self.occurrence_table[action] / sum(self.occurrence_table[action])
        return np.random.choice([s for s in range(self.num_states)], 1, p=probabilities)[0]


class NeighbourAgent:

    def __init__(self):
        """
        Initialize neighbour agent class.
        """
        self.occurrence_table = self.initialize_prior_occurrence_table()
        self.num_actions, self.num_states = self.occurrence_table.shape

    @staticmethod
    def initialize_prior_occurrence_table() -> np.ndarray:
        """
        Initialize prior occurrence table $V_{o_{\M{a}}}$.
        """
        return N_VEC.copy()


class BaseAgent:

    def __init__(self):
        """
        Initialize agent class without information fusion. This agent demonstrates a benchmark.
        """
        self.occurrence_table = self.initialize_prior_occurrence_table()
        self.num_actions, self.num_states = self.occurrence_table.shape

    @staticmethod
    def initialize_prior_occurrence_table() -> np.ndarray:
        """
        Initialize prior occurrence table $V_{o_{\M{a}}}$.
        """
        return A_VEC.copy()

    def update_occurrence_table(self, action: int, state: int) -> None:
        """
        Updates the occurrence table during each decision step.
        """
        self.occurrence_table[action][state] += 1


class FusionAgent:

    def __init__(self):
        """
        Initialize agent class with information fusion $\M{a}$.
        """
        self.occurrence_table = self.initialize_prior_occurrence_table()
        self.num_actions, self.num_states = self.occurrence_table.shape

    @staticmethod
    def initialize_prior_occurrence_table() -> np.ndarray:
        """
        Initialize prior occurrence table $V_{o_{\M{a}}}$.
        """
        return A_VEC.copy()

    def update_occurrence_table(self, action: int, state: int) -> None:
        """
        Update the occurrence table during each decision step.
        """
        self.occurrence_table[action][state] += 1

    def update_occurrence_table_with_fusion(self, ext_agent: NeighbourAgent, w: float, tau: int) -> None:
        for action in range(self.num_actions):
            self.occurrence_table[action] += tau * w * (ext_agent.occurrence_table[action] / sum(ext_agent.occurrence_table[action]))
