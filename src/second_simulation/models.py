from typing import Optional

import numpy as np

# CASE 1
# ENVIRONMENT = np.array(
#     [[[0, 1], [1, 1]],
#      [[1, 1], [1, 0]]],
#     dtype=float
# )
#
# NEIGHBOUR = np.array(
#     [[1, 1e-6],
#      [1e-6, 1]],
#     dtype=float
# )
#
# AGENT = np.array(
#     [[1e-6, 1],
#      [1, 1e-6]],
#     dtype=float
# )

# CASE 2
ENVIRONMENT = np.array(
    [[[1, 4], [2, 3]],
     [[2, 3], [9, 1]]],
    dtype=float
)

NEIGHBOUR = np.array(
    [[1, 2],
     [1, 1]],
    dtype=float
)

AGENT = np.array(
    [[2, 1],
     [1, 3]],
    dtype=float
)


class Environment:

    def __init__(self):
        """
        Initialize environment class.
        """
        self.occurrence_table = ENVIRONMENT.copy()
        self.num_dt, _, _ = self.occurrence_table.shape

    def dt(self, dt_1: int, dt_2: int, rng: Optional[np.random.Generator] = None) -> int:
        """
        Generate and return an observable dt.
        """
        probabilities = self.occurrence_table[dt_2][dt_1] / sum(self.occurrence_table[dt_2][dt_1])
        return rng.choice([dt for dt in range(self.num_dt)], 1, p=probabilities)[0]


class NeighbourAgent:

    def __init__(self):
        """
        Initialize neighbour agent class.
        """
        self.occurrence_table = NEIGHBOUR.copy()

    def update_occurrence_table(self, dt: int, dt_2: int) -> None:
        """
        Updates the occurrence table during each time step.
        """
        self.occurrence_table[dt_2][dt] += 1

    def dt(self, dt_2: int) -> int:
        """
        Generate and return an observable dt.
        """
        probabilities = self.occurrence_table[dt_2] / sum(self.occurrence_table[dt_2])
        return max(enumerate(probabilities), key=lambda x: x[1])[0]


class BaseAgent:

    def __init__(self):
        """
        Initialize agent class without information fusion. This agent demonstrates a benchmark.
        """
        self.occurrence_table = AGENT.copy()

    def update_occurrence_table(self, dt: int, dt_1: int) -> None:
        """
        Updates the occurrence table during each time step.
        """
        self.occurrence_table[dt_1][dt] += 1

    def dt(self, dt_1: int) -> int:
        """
        Generate and return an observable dt.
        """
        probabilities = self.occurrence_table[dt_1] / sum(self.occurrence_table[dt_1])
        return max(enumerate(probabilities), key=lambda x: x[1])[0]


class FusionAgent:

    def __init__(self):
        """
        Initialize agent class with information fusion $\M{a}$.
        """
        self.occurrence_table = AGENT.copy()

    def update_occurrence_table(self, dt: int, dt_1: int) -> None:
        """
        Update the occurrence table during each time step.
        """
        self.occurrence_table[dt_1][dt] += 1

    def update_occurrence_table_with_fusion(self, ext_agent: NeighbourAgent, w: float, dt_1: int) -> None:
        """
        Update the occurrence table during each time step.
        """
        self.occurrence_table[dt_1] += w * (ext_agent.occurrence_table[dt_1] / sum(ext_agent.occurrence_table[dt_1]))

    def dt(self, dt_1: int) -> int:
        """
        Generate and return an observable dt.
        """
        probabilities = self.occurrence_table[dt_1] / sum(self.occurrence_table[dt_1])
        return max(enumerate(probabilities), key=lambda x: x[1])[0]
