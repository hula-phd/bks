from pathlib import Path
from typing import Dict, List

import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

from src.second_simulation.models import BaseAgent, Environment, FusionAgent, NeighbourAgent

# Simulation settings
SEED = 1
W_STEP = .02
W_MAX = 1


class SecondExperiment:

    def __init__(self, num_steps: int, num_mc_runs: int) -> None:
        """
        Initialize experiment.
        """
        self.w_span = np.arange(0, W_MAX + W_STEP, W_STEP)
        self.num_steps = num_steps
        self.num_mc_runs = num_mc_runs

    def run(self) -> None:
        """
        Run experiment.
        """
        results = {}
        for w in self.w_span:
            rng = np.random.default_rng(SEED)
            results[w] = self._get_predictions(w, rng)
        self.plot_results(results)

    def plot_results(self, results: Dict[float, List]) -> None:
        """
        Plot figures.
        """
        fontsize = 16
        base_agent_kld = []
        agent_kld = []
        ext_kld = []

        for w in self.w_span:
            res = self._compute_precision(results[w])
            agent_kld.append(res[0] * 100)
            base_agent_kld.append(res[1] * 100)
            ext_kld.append(res[2] * 100)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(self.w_span, agent_kld, c='r', ls='-', linewidth=4)
        ax.plot(self.w_span, base_agent_kld, c='b', ls='--', linewidth=4)
        ax.plot(self.w_span, ext_kld, c='g', ls=':', linewidth=4)
        plt.xlabel(r'$\mathsf{w}$', fontsize=fontsize)
        plt.ylabel('Accuracy [%]', fontsize=fontsize)
        plt.xticks(fontsize=fontsize)
        plt.yticks(fontsize=fontsize)
        plt.grid()
        plt.tight_layout()
        plt.savefig(Path(__file__).parent / f'figures/results.png')
        plt.show()

    def _get_predictions(self, w: float, rng: np.random.Generator) -> List:
        """
        Return predictions for both agents and environment with respect to all time steps.
        """
        return [MonteCarloSimulation(w, self.num_steps).perform_single_run(rng) for _ in tqdm(range(self.num_mc_runs))]

    def _compute_precision(self, results: List) -> List[float]:
        """
        Compute precision accuracy. 
        """
        agent_precision, base_agent_precision, external_agent_precision = 0, 0, 0
        for time_slice in results:
            for i in time_slice:
                if i[0] == i[1]:
                    agent_precision += 1
                if i[0] == i[2]:
                    base_agent_precision += 1
                if i[0] == i[3]:
                    external_agent_precision += 1
        return [self._normalize_precision(agent_precision), self._normalize_precision(base_agent_precision), self._normalize_precision(external_agent_precision)]

    def _normalize_precision(self, precision: int) -> float:
        """
        Normalize precision with respect to the total number of time steps and the total number of Monte Carlo runs.
        """
        return precision / (self.num_steps * self.num_mc_runs)


class MonteCarloSimulation:

    def __init__(self, w: float, num_steps: int) -> None:
        """
        Initialize a single Monte Carlo simulation.
        """
        self.w = w
        self.num_steps = num_steps
        self.environment = Environment()
        self.ext_agent = NeighbourAgent()
        self.base_agent = BaseAgent()
        self.agent = FusionAgent()

    def perform_single_run(self, rng: np.random.Generator) -> np.ndarray:
        """
        Perform a single run for the given number of time steps and return predictions.
        """
        predictions = np.zeros((self.num_steps, 4), dtype=int)

        dt_1, dt_2 = 0, 1
        for step in range(self.num_steps):
            # Environment generates new data
            dt = self.environment.dt(dt_1=dt_1, dt_2=dt_2, rng=rng)

            # Bayesian update
            self.ext_agent.update_occurrence_table(dt=dt, dt_2=dt_2)
            self.base_agent.update_occurrence_table(dt=dt, dt_1=dt_1)

            self.agent.update_occurrence_table(dt=dt, dt_1=dt_1)
            self.agent.update_occurrence_table_with_fusion(ext_agent=self.ext_agent, w=self.w, dt_1=dt_1)

            # Predictions
            predictions[step] = [dt, self.agent.dt(dt_1=dt_1), self.base_agent.dt(dt_1=dt_1), self.ext_agent.dt(dt_2=dt_2)]

            # Swap previous data
            dt_2, dt_1 = dt_1, dt
        return predictions
